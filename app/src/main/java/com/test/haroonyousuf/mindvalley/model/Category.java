package com.test.haroonyousuf.mindvalley.model;

public class Category {

    private Long id;
    private String title;
    private Long photo_count;
    private CategoryLinks links;

    /**
     * @return The id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The photo_count
     */
    public Long getPhotoCount() {
        return photo_count;
    }

    /**
     * @param photoCount The photo_count
     */
    public void setPhotoCount(Long photoCount) {
        this.photo_count = photoCount;
    }

    /**
     * @return The links
     */
    public CategoryLinks getLinks() {
        return links;
    }

    /**
     * @param links The links
     */
    public void setLinks(CategoryLinks links) {
        this.links = links;
    }

}
