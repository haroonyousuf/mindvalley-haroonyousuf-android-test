package com.test.haroonyousuf.mindvalley.model;

public class UserLinks {

    private String self;
    private String html;
    private String photos;
    private String likes;

    /**
     * @return The self
     */
    public String getSelf() {
        return self;
    }

    /**
     * @param self The self
     */
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     * @return The html
     */
    public String getHtml() {
        return html;
    }

    /**
     * @param html The html
     */
    public void setHtml(String html) {
        this.html = html;
    }

    /**
     * @return The photos
     */
    public String getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(String photos) {
        this.photos = photos;
    }

    /**
     * @return The likes
     */
    public String getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    public void setLikes(String likes) {
        this.likes = likes;
    }

}
