package com.test.haroonyousuf.mindvalley.ui.pinboard;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.test.haroonyousuf.mindvalley.R;
import com.test.haroonyousuf.mindvalley.Utils.Constants;
import com.test.haroonyousuf.mindvalley.adapters.PinboardRecyclerViewAdapter;
import com.test.haroonyousuf.mindvalley.api.MVService;
import com.test.haroonyousuf.mindvalley.model.PinItem;
import com.test.haroonyousuf.mindvalley.ui.BaseFragment;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import butterknife.Bind;


/**
 * A placeholder fragment containing a simple view.
 */
public class PinboardActivityFragment extends BaseFragment implements PinboardRecyclerViewAdapter.OnListFragmentInteractionListener {
    public static final String LOG_TAG = PinboardActivityFragment.class.getSimpleName();

    //Todo: move this to constants file
    public static final String REQEUST_PARAM = "wgkJgazE";


    /**
     * Layout views
     */
    @Bind(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.fab)
    FloatingActionButton mFab;

    @Bind(R.id.list)
    RecyclerView mRecyclerView;

    /**
     * private variables
     */
    List<PinItem> mLstPhotos = new ArrayList<>();
    PinboardRecyclerViewAdapter mRecyclerViewAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pinboard_item_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerViewAdapter = new PinboardRecyclerViewAdapter(mLstPhotos, this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Constants.GRID_LAYOUT_COLUMNS_COUNT));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, getString(R.string.dummy_fab_click), Snackbar.LENGTH_LONG).show();

            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                /* send request to server to fetch data */
                getAllPhotos(REQEUST_PARAM);
            }
        });

        getAllPhotos(REQEUST_PARAM);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    /**
     * @param item object of clicked item
     */
    @Override
    public void onListItemClickListener(PinItem item) {
        Toast.makeText(getActivity(), getString(R.string.dummy_item_click, item.getUser().getName()), Toast.LENGTH_SHORT).show();
    }



    /**
     * request photos from service
     *
     * @param key photos primary key
     */
    private void getAllPhotos(String key) {

        Call<List<PinItem>> call = MVService.getMVClient().getPhotos(key);

        call.enqueue(new Callback<List<PinItem>>() {
            @Override
            public void onResponse(Call<List<PinItem>> call, Response<List<PinItem>> response) {
                if (response.isSuccessful()) {
                    mLstPhotos.clear();
                    for(PinItem photo: response.body()){
                        mLstPhotos.add(photo);
                    };
                    mRecyclerViewAdapter.notifyDataSetChanged();

                } else {
                    Log.e(LOG_TAG, "GetPhotos: Response not successful, response code:" + response.code());
                }

                /* hide refresh layout progress indicator */
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<PinItem>> call, Throwable t) {

                /* hide refresh layout progress indicator */
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e(LOG_TAG, "GetPhotos: Response not successful, response code");
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRecyclerViewAdapter.clean();
    }
}
