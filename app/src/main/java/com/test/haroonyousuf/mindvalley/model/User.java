package com.test.haroonyousuf.mindvalley.model;

public class User {

    private String id;
    private String username;
    private String name;
    private ProfileImage profile_image;
    private UserLinks links;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The profile_image
     */
    public ProfileImage getProfile_image() {
        return profile_image;
    }

    /**
     * @param profile_image The profile_image
     */
    public void setProfile_image(ProfileImage profile_image) {
        this.profile_image = profile_image;
    }

    /**
     * @return The links
     */
    public UserLinks getLinks() {
        return links;
    }

    /**
     * @param links The links
     */
    public void setLinks(UserLinks links) {
        this.links = links;
    }

}
