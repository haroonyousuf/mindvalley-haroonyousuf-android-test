package com.test.haroonyousuf.mindvalley.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.test.haroonyousuf.mindvalley.R;
import com.test.haroonyousuf.mindvalley.model.PinItem;
import com.test.haroonyousuf.mindvalley.model.User;
import com.test.haroonyousuf.mvlib.MVLib;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class PinboardRecyclerViewAdapter extends RecyclerView.Adapter<PinboardRecyclerViewAdapter.ViewHolder> {

    private List<PinItem> mValues; //list to hold pin items
    private OnListFragmentInteractionListener mListener; //pin item listener

    public PinboardRecyclerViewAdapter(List<PinItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pinboard_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        User user = holder.mItem.getUser();
        String thumbnailURL = holder.mItem.getUrls().getThumb();
        String userSmallProfileImageURL = user.getProfile_image().getSmall();

        holder.mName.setText(user.getName());

        /* load thumbnail using mindvalley lib  */
        MVLib.with(holder.mThumbnail.getContext()).loadResource(thumbnailURL, holder.mThumbnail);

        /* load user profile image using mindvalley lib */
        MVLib.with(holder.mDisplayImage.getContext()).loadResource(userSmallProfileImageURL, holder.mDisplayImage);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    /* Notify the active callbacks interface ( activity/fragment ) that an item has been clicked. */
                    mListener.onListItemClickListener(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    /**
     * Hold layout views
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mName;
        public final ImageView mThumbnail;
        public final CircleImageView mDisplayImage;
        public PinItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mName = (TextView) view.findViewById(R.id.user);
            mThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            mDisplayImage = (CircleImageView) view.findViewById(R.id.display_pic);
        }

    }

    /**
     * Interface to handle item click
     */
    public interface OnListFragmentInteractionListener {
        void onListItemClickListener(PinItem item);
    }

    /* Cleanup the resources when fragment would destroy */
    public void clean(){

        mValues.clear();
        mValues = null;
        mListener = null;
    }
}
