package com.test.haroonyousuf.mindvalley;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

/**
 * Created by haroonyousuf on 7/18/16.
 */
public class MindValleyApplication extends Application {

    private RefWatcher mRefWatcher;
    private static Context mContext;

    public static MindValleyApplication get(Context context) {
        return (MindValleyApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mRefWatcher = installLeakCanary();
    }

    public static Context getContext() {
        return mContext;
    }

    public RefWatcher getRefWatcher() {
        return mRefWatcher;
    }

    protected RefWatcher installLeakCanary() {
        return LeakCanary.install(this);
    }
}
