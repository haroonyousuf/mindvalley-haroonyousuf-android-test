package com.test.haroonyousuf.mindvalley.model;

public class CategoryLinks {

    private String self;
    private String photos;

    /**
     * @return The self
     */
    public String getSelf() {
        return self;
    }

    /**
     * @param self The self
     */
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     * @return The photos
     */
    public String getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(String photos) {
        this.photos = photos;
    }
}
