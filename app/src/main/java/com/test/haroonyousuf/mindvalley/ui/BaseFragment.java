package com.test.haroonyousuf.mindvalley.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.view.View;

import com.test.haroonyousuf.mindvalley.MindValleyApplication;

import butterknife.ButterKnife;

public class BaseFragment extends Fragment {

    @CallSuper
    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @CallSuper
    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @CallSuper
    @Override public void onDestroy() {
        super.onDestroy();
        MindValleyApplication.get(getActivity()).getRefWatcher().watch(this);
    }
}
