package com.test.haroonyousuf.mindvalley.api;

import com.test.haroonyousuf.mindvalley.Utils.Constants;
import com.test.haroonyousuf.mindvalley.model.PinItem;
import java.util.List;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by haroonyousuf on 7/18/16.
 */
public class MVService {

    private static final String LOG_TAG = MVService.class.getSimpleName();
    private static MVApiInterface mMVService;

    public static MVApiInterface getMVClient() {

        if (mMVService == null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            /* Create intercepter for logging */
            OkHttpClient httpClient = new OkHttpClient();
            httpClient.newBuilder().interceptors().add(logging);

            Retrofit restAdapter = new Retrofit.Builder()
                    .baseUrl(Constants.SERVICE_END_POINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            mMVService = restAdapter.create(MVApiInterface.class);
        }
        return mMVService;
    }

    /* Mindvalley Api interface */
    public interface MVApiInterface {
        /**
         * Get the list of photos.
         *
         * @param key page index
         */
        @GET("raw/{key}")
        Call<List<PinItem>> getPhotos(@Path("key") String key);

    }
}

