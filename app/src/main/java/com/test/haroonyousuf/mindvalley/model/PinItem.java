package com.test.haroonyousuf.mindvalley.model;

import java.util.ArrayList;
import java.util.List;

public class PinItem {

    private String id;
    private String created_at;
    private Long width;
    private Long height;
    private String color;
    private Long likes;
    private Boolean liked_by_user;
    private User user;
    private List<Object> current_user_collections = new ArrayList<Object>();
    private Urls urls;
    private List<Category> categories = new ArrayList<Category>();
    private PhotoLinks links;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreatedAt() {
        return created_at;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.created_at = createdAt;
    }

    /**
     *
     * @return
     * The width
     */
    public Long getWidth() {
        return width;
    }

    /**
     *
     * @param width
     * The width
     */
    public void setWidth(Long width) {
        this.width = width;
    }

    /**
     *
     * @return
     * The height
     */
    public Long getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */
    public void setHeight(Long height) {
        this.height = height;
    }

    /**
     *
     * @return
     * The color
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     * The likes
     */
    public Long getLikes() {
        return likes;
    }

    /**
     *
     * @param likes
     * The likes
     */
    public void setLikes(Long likes) {
        this.likes = likes;
    }

    /**
     *
     * @return
     * The liked_by_user
     */
    public Boolean getLikedByUser() {
        return liked_by_user;
    }

    /**
     *
     * @param likedByUser
     * The liked_by_user
     */
    public void setLikedByUser(Boolean likedByUser) {
        this.liked_by_user = likedByUser;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The current_user_collections
     */
    public List<Object> getCurrentUserCollections() {
        return current_user_collections;
    }

    /**
     *
     * @param currentUserCollections
     * The current_user_collections
     */
    public void setCurrentUserCollections(List<Object> currentUserCollections) {
        this.current_user_collections = current_user_collections;
    }

    /**
     *
     * @return
     * The urls
     */
    public Urls getUrls() {
        return urls;
    }

    /**
     *
     * @param urls
     * The urls
     */
    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    /**
     *
     * @return
     * The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     *
     * @param categories
     * The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     *
     * @return
     * The links
     */
    public PhotoLinks getLinks() {
        return links;
    }

    /**
     *
     * @param links
     * The links
     */
    public void setLinks(PhotoLinks links) {
        this.links = links;
    }

}
