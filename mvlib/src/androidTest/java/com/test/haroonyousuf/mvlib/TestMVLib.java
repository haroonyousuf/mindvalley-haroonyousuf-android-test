package com.test.haroonyousuf.mvlib;

import android.test.AndroidTestCase;


/**
 * Created by haroonyousuf on 7/17/16.
 */
public class TestMVLib extends AndroidTestCase {

    /* Sorry! time is short. Will complete it later. :( */

    public void testLoadResourceShouldReturnXML() throws Exception {
        //arrange
        String data;

        //act
        data = MVLib.with(getContext()).loadResource("http://dummy.com/res/json", MVLib.XML);

        //assert
        assertEquals(data, "{a:1}");
    }

    public void testLoadResourceShouldReturnJSON() throws Exception {
        //arrange
        String data;

        //act
        data = MVLib.with(getContext()).loadResource("http://dummy.com/res/xml", MVLib.XML);

        //assert
        assertEquals(data, "<a>1</a>");
    }
}
