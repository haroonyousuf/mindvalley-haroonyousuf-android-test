package com.test.haroonyousuf.mvlib;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import java.util.Objects;

public class ResourceHandler{


    private final Context context;
    LruCache<String, Object> bitmapCache;


    public ResourceHandler(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.context = context.getApplicationContext();
    }

    /**
     * Create the {@link MVLib} instance.
     */
    public MVLib build() {
        Context context = this.context;

        if (bitmapCache == null) {
            // Get max available VM memory, exceeding this amount will throw an
            // OutOfMemory exception. Stored in kilobytes as LruCache takes an
            // int in its constructor.
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = maxMemory / 8;

            //int cacheSize = 4 * 1024 * 1024; // 4MiB
            bitmapCache = new LruCache<String, Object>(cacheSize) {
                protected int sizeOf(String key, Bitmap value) {
                    // The cache size will be measured in kilobytes rather than
                    // number of items.
                    return value.getByteCount() / 1024;
                }
            };
        }

        return new MVLib(context, bitmapCache);

    }

}
