package com.test.haroonyousuf.mvlib;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;


import java.io.InputStream;
import java.lang.ref.WeakReference;

/**
 * API for building an resource download request.
 */
public class MVLib {

    /* hold singleton instance */
    static volatile MVLib singleton = null;

    /* cache store to hold any kind of data */
    LruCache<String, Object> cache;
    Context context;


    /**
     * This loads {@code IMAGE} resource
     */
    public static final int IMAGE = 0;

    /**
     * This loads {@code JSON} resource
     */
    public static final int JSON = 1;

    /**
     * This loads {@code XML} resource
     */
    public static final int XML = 2;


    MVLib(Context context, LruCache<String, Object> bitmapCache) {
        this.context = context;
        this.cache = bitmapCache;
    }


    /**
     * @param context activity/fragment context
     * @return library singleton object
     */
    public static MVLib with(Context context) {
        if (singleton == null) {
            synchronized (ResourceHandler.class) {
                if (singleton == null) {
                    singleton = new ResourceHandler(context).build();
                }
            }
        }
        return singleton;
    }


    /**
     * @param key  cached object identifier. In our case its {@code URL}.
     * @param resp any kind of response i.e {@code Bitmap},{@code XML},{@code JSON}.
     */
    private void addResponseToMemCache(String key, Object resp) {
        if (getResponseFromMemCache(key) == null && resp != null) {
            cache.put(key, resp);
        }
    }

    /**
     * @param key cached object identifier. In our case its {@code URL}.
     * @return return object at specified key
     */
    private Object getResponseFromMemCache(String key) {
        return cache.get(key);
    }


    /**
     * Start an resource request using the specified path. This is a convenience method for calling
     * {@link #loadResource(String path, int resourceType)}.
     * <p/>
     * This path must be a remote URL.
     * <p/>
     * Passing {@code null} or {@code empty} as a {@code path} will not trigger any request.
     *
     * @param path         remote URL.
     * @param resourceType One of {@link #JSON}, {@link #XML}.
     * @return
     * @throws IllegalArgumentException if {@code path} is empty, null or blank string.
     */
    public String loadResource(String path, int resourceType) {

        if (path == null || path.trim().length() == 0) {
            throw new IllegalArgumentException("Path must not be empty.");

        } else {

            final String imageKey = String.valueOf(path);

            try {
                String resource = (String) getResponseFromMemCache(imageKey);
                if (resource != null) {
                    return resource;
                } else {

                /* Asynchronously load the JSON/XML and store it to cache. Same like image bitmap. */
                    switch (resourceType) {
                        case JSON:
                            return "{a:1}"; //sorry! can't finish it due to time constraint :(
                        case XML:
                            return "<a>1</a>";
                    }
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }

        return null;
    }


    /**
     * Load the image if its exist in cache store otherwise request asynchronously load an image.
     * Save the image to cache store if not present
     *
     * @param path      image url
     * @param imageView view to load image
     */
    public void loadResource(String path, ImageView imageView) {
        if (cancelPotentialWork(path, imageView)) {


            final String imageKey = String.valueOf(path);

            try {
                final Bitmap bitmap = (Bitmap) getResponseFromMemCache(imageKey);
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    //imageView.setImageResource(R.drawable.image_placeholder);
                    BitmapWorkerTask task = new BitmapWorkerTask(imageView);
                    task.execute(path);
                }
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * BitmapWorkerTask to asynchronously load the remote bitmap resource
     */
    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;
        private String data = "";

        public BitmapWorkerTask(ImageView imageView) {
            /* Use a WeakReference to ensure the ImageView can be garbage collected */
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        /* Decode image in background. */
        @Override
        protected Bitmap doInBackground(String... params) {
            data = params[0];

            final Bitmap bitmap = decodeSampledBitmapFromStream(params[0]);
            addResponseToMemCache(String.valueOf(params[0]), bitmap);
            return bitmap;
        }

        /* Once complete, see if ImageView is still around and set bitmap. */
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    /**
     * Added for future use. Placeholder image can be displayed in the ImageView while the task completes.
     */
    static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }


    /**
     * @param path      resource {@code URL}
     * @param imageView view to load image
     * @return {@code false} if same work is already in progress
     */
    private static boolean cancelPotentialWork(String path, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.data;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData.equals("") || bitmapData.equals(path)) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }


    /**
     * @param path remote {@code URL}
     * @return decoded bitmap
     */
    private static Bitmap decodeSampledBitmapFromStream(String path) {
        Bitmap mBitmap = null;

        try {
            InputStream in = new java.net.URL(path).openStream();
            mBitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        return mBitmap;
    }
}
