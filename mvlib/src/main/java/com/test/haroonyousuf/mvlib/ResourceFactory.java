package com.test.haroonyousuf.mvlib;

/**
 * Created by haroonyousuf on 7/16/16.
 */
public class ResourceFactory {

    //use getShape method to get object of type shape
    public Resource getResource(int resourceType){

        switch (resourceType){
            case MVLib.IMAGE:
                return  new ImageResource();
            case MVLib.JSON:
                return  new JSONResource();
            case MVLib.XML:
                return  new XmlResource();
        }

        return null;
    }
}
