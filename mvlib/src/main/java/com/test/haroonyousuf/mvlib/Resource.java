package com.test.haroonyousuf.mvlib;

/**
 * Created by haroonyousuf on 7/16/16.
 */
public interface Resource {
    void loadData();
}
